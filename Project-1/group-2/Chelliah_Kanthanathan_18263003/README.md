
Project 1

Student Name: Chelliah Kanthanathan
Student Id: 18263003

My_AI_Demo.process - The Top level AI that calculates the score for cells

FindCellType.process - The Sub AI that finds the cell type and return 3 outputs Corner, Edge and Middle

Default scores / Ratings - Corner cell - 30, Edge cell - 20, Middle cell - 10 (30 being highest and 10 lowest)

Strategy - Always select Corner cell first followed by Edge cell and then the Middle cell. Before selecting Edge or Middle always check if the cell is critical and also endangered. If both are true then assign highest score to select this cell to explode and occupy enemy cells, In case if Edge and Middle cells get same highest score 25, then the Edge is always preferred before Middle as it needs only 3 atoms to explode. The idea is to start occupying the cells in the order Corner followed by Edge and then Middle based on if cell is critical and endangered or not.

Main Logic:
Corner Cell - Check if Critical, False - then assign highest score 30, True - then assign 28 next possible highest score
Edge Cell - Check if Critical True and if Endangered True - then assign score 25, Check if Critical True and if Endangered False - then assign score 22
Middle Cell -  Check if Critical True and if Endangered True - then assign score 25, Check if Critical True and if Endangered False - then assign score 12